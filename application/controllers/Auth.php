<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('m_api');
    }
    
    public function login()
    {
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $arrayall = array(
                'status' => 'error',
                'message' => validation_errors_array()
            );
        }
        else
        {
            $email = $this->input->post('email'); 
            $password = md5($this->input->post('password')); 

            $cek = $this->m_api->login_num($email, $password);
            
            if($cek != 0){
                
                $get = $this->m_api->login($email, $password);
                
                $data = array(
                        'logged' => TRUE,
                        'id' => simple_encrypt($get['id_admin']),
                        'level' => $get['level']
                    );
                
                $this->session->set_userdata($data);      
                
                $arrayall = array( 'status' => 'success', 'message' => 'Login Success' );
            }else{
                $arrayall = array( 'status' => 'error', 'message' => 'Login error' );
            }
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    function logout() 
    {
        $this->session->sess_destroy();
        redirect(site_url('login'));
        
    }
}

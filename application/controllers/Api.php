<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public $title = 'API';
    public function __construct() {
        parent::__construct();
        $this->load->model('m_api');
    }

    public function mobil()
    {
        $merek = $this->uri->segment(3);
        $tipe = $this->uri->segment(4);
        
        if(!isset($merek)){
            
        $q = $this->m_api->mobil();
            
        } else if(!isset($tipe)) {
            
            $q = $this->m_api->mobil_by_merek($merek);
            
        } else {
            $q = $this->m_api->mobil_by_merek_type($merek, $tipe);
        }
        
        $arr = array();
        foreach($q as $row):
        
        $arra = array(
            'id' => simple_encrypt($row['id_mobil']),
            'create_date' => $row['create_date'],
            'no_kerangka' => $row['no_kerangka'],
            'no_polisi' => $row['no_polisi'],
            'merek' => $row['merek'],
            'tipe' => $row['tipe'],            
            'tahun' => $row['tahun']
        );

        array_push($arr,$arra);
        endforeach;
        
        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;        
             
    }

    public function mobil_new()
    {
        $this->form_validation->set_rules('no_kerangka', 'Nomor Kerangka', 'required');    
        $this->form_validation->set_rules('no_polisi', 'Nomor Polisi', 'required');    
        $this->form_validation->set_rules('merek', 'Merek', 'required');    
        $this->form_validation->set_rules('tipe', 'Tipe', 'required');    
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');    

        if ($this->form_validation->run() == FALSE)
        {
            $arrayall = array(
                'status' => 'error',
                'message' => validation_errors_array()
            );
        }
        else
        {               
            $data = array(
                'no_kerangka' => $this->input->post('no_kerangka'),
                'no_polisi' => $this->input->post('no_polisi'),
                'merek' => $this->input->post('merek'),
                'tipe' => $this->input->post('tipe'),
                'tahun' => $this->input->post('tahun')
            );

            $insert = $this->m_crud->insert('mobil', $data);

            if($insert == 1){
                $arrayall = array( 'status' => 'success', 'message' => 'Insert data success'  );
            }else{
                $arrayall = array( 'status' => 'error', 'message' => 'Insert data error' );
            }
        }
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;            
    }
    
    public function mobil_edit()
    {
        $this->form_validation->set_rules('no_kerangka', 'Nomor Kerangka', 'required');    
        $this->form_validation->set_rules('no_polisi', 'Nomor Polisi', 'required');    
        $this->form_validation->set_rules('merek', 'Merek', 'required');    
        $this->form_validation->set_rules('tipe', 'Tipe', 'required');    
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');    

        if ($this->form_validation->run() == FALSE)
        {
            $arrayall = array(
                'status' => 'error',
                'message' => validation_errors_array()
            );
        }
        else
        {   
             $id = simple_decrypt($this->input->post('id'));
            
            $data = array(
                'no_kerangka' => $this->input->post('no_kerangka'),
                'no_polisi' => $this->input->post('no_polisi'),
                'merek' => $this->input->post('merek'),
                'tipe' => $this->input->post('tipe'),
                'tahun' => $this->input->post('tahun')
            );

            $insert = $this->m_crud->update('mobil','id_mobil', $data, $id);

            if($insert == 1){
                $arrayall = array( 'status' => 'success', 'data' => $data, 'message' => 'Update data success'  );
            }else{
                $arrayall = array( 'status' => 'error', 'message' => 'Update data error' );
            }
        }
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;            
    }
    
    public function mobil_delete()
    {
        $this->form_validation->set_rules('id', 'ID', 'required');    

        if ($this->form_validation->run() == FALSE)
        {
            $arrayall = array(
                'status' => 'error',
                'message' => validation_errors_array()
            );
        }
        else
        {   
            $id = simple_decrypt($this->input->post('id'));
            
            $insert = $this->m_crud->delete('mobil','id_mobil', $id);

            if($insert == 1){
                $arrayall = array( 'status' => 'success', 'message' => 'Delete data success'  );
            }else{
                $arrayall = array( 'status' => 'error', 'message' => 'Delete data error' );
            }
        }
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;            
    }
    
    public function saveMobil()
    {
          $data = (array)json_decode(file_get_contents('php://input'));
          $this->m_api->insertMobil($data);

          $response = array(
            'status' => 'success',
            'Info' => 'Data Tersimpan');

          $this->output
            ->set_status_header(201)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
            exit;
      }

    public function updateMobil($id)
    {
        $id = simple_decrypt($id);
        
        $data = (array)json_decode(file_get_contents('php://input'));
        $this->m_api->updateMobil($data, $id);

        $response = array(
          'status' => 'success',
          'Info' => 'Data Berhasil di update');

        $this->output
          ->set_status_header(200)
          ->set_content_type('application/json', 'utf-8')
          ->set_output(json_encode($response, JSON_PRETTY_PRINT))
          ->_display();
          exit;
      }

    public function deleteMobil($id)
    {
        $id = simple_decrypt($id);
        
        $this->m_api->deleteMobil($id);

        $response = array(
          'status' => 'success',
          'Info' => 'Data Berhasil di hapus');

        $this->output
          ->set_status_header(200)
          ->set_content_type('application/json', 'utf-8')
          ->set_output(json_encode($response, JSON_PRETTY_PRINT))
          ->_display();
          exit;
      }    
    
}

?>
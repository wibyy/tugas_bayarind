<?php
class M_api extends CI_Model {

    public function mobil() {
        $this->db->order_by('create_date','DESC');
        $sql = $this->db->get('mobil');
        return $sql->result_array();
    }
    
    public function mobil_by_merek($merek)
    {
        $this->db->where('merek', $merek);
        $this->db->order_by('create_date','DESC');
        $sql = $this->db->get('mobil');
        return $sql->result_array();
    }    
    public function mobil_by_merek_type($merek, $tipe)
    {
        $this->db->where('merek', $merek);
        $this->db->where('tipe', $tipe);
        $this->db->order_by('create_date','DESC');
        $sql = $this->db->get('mobil');
        return $sql->result_array();
    }    
    
    public function insertMobil($dataMobil)
      {
          $val = array(
            'no_kerangka' => $dataMobil['no_kerangka'],
            'no_polisi' => $dataMobil['no_polisi'],
            'merek' => $dataMobil['merek'],
            'tipe' => $dataMobil['tipe'],
            'tahun' => $dataMobil['tahun']
          );
          $this->db->insert('mobil', $val);
      }

    public function updateMobil($dataMobil, $id)
      {
        $val = array(
            'no_kerangka' => $dataMobil['no_kerangka'],
            'no_polisi' => $dataMobil['no_polisi'],
            'merek' => $dataMobil['merek'],
            'tipe' => $dataMobil['tipe'],
            'tahun' => $dataMobil['tahun']
        );
        $this->db->where('id_mobil', $id);
        $this->db->update('mobil', $val);
      }

    public function deleteMobil($id)
      {
        $val = array(
          'id_mobil' => $id
        );
        $this->db->delete('mobil', $val);
      }    
    
}

?>
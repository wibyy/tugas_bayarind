<?php

class M_global extends CI_Model {

    function get_all($table) {
        $query = $this->db->get($table);
        return $query->result_array();
    }
    
    function get_all_order($table,$field,$opsi) {
        $this->db->order_by($field,$opsi);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function get_by_id($table, $field, $id) {
        $query = $this->db->get_where($table, array($field => $id));
        return $query->row_array();
    }
    function get_list_by_id($table, $field, $id) {
        $query = $this->db->get_where($table, array($field => $id));
        return $query->result_array();
    }
    function get_list_by_id_order($table, $field, $id,$fieldo,$opsi) {
        $this->db->order_by($fieldo,$opsi);
        $query = $this->db->get_where($table, array($field => $id));
        return $query->result_array();
    }
    
    function num($table) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    
    function num_by_id($table, $field, $id) {
        $query = $this->db->get_where($table, array($field => $id));
        return $query->num_rows();
    }
    

    function get_limit($table, $limit) {
        $query = $this->db->limit($limit)->get_where($table);
        return $query->result_array();
    }
    function get_limit_order($table, $limit,$field,$opsi) {
        $this->db->order_by($field,$opsi);
        $query = $this->db->limit($limit)->get_where($table);
        return $query->result_array();
    }

    function get_by_limit($table, $field, $id, $limit) {
        $query = $this->db->limit($limit)->get_where($table, array($field => $id));
        return $query->result();
    }

    function get_by_status($table, $field, $status) {
        $query = $this->db->get_where($table, array($field => $status));
        return $query->result();
    }

    function get_by_status_arr($table, $field, $status) {
        $query = $this->db->where_in($field, $status)->get($table);
        return $query->result();
    }

    function get_by_id_not_in($table, $field, $id, $not, $not_id) {
        $query = $this->db->where_not_in($not, $not_id)->get_where($table, array($field => $id));
        return $query->result();
    }

    function get_select($table, $field, $id) {
        $query = $this->db->where_not_in($field, $id)->get($table);
        return $query->result();
    }

    function get_select_in_id($table, $field, $id, $in, $in_id) {
        $query = $this->db->where_not_in($field, $id)->where($in, $in_id)->get($table);
        return $query->result();
    }

    function set_status($table, $field, $id, $stat, $status) {
        $this->db->set($stat, $status);
        $this->db->where($field, $id);
        $this->db->update($table);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }
    }

    function check_exist($table, $field, $value) {
        $query = $this->db->get_where($table, array($field => $value), 1, 0);

        if ($query->num_rows() > 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }	
    }




      function bulan($bln){
        $bulan = $bln;
            switch ($bulan){
             case 1 : $bulan="Januari";
             Break;
             case 2 : $bulan="Februari";
             Break;
             case 3 : $bulan="Maret";
             Break;
             case 4 : $bulan="April";
             Break;
             case 5 : $bulan="Mei";
             Break;
             case 6 : $bulan="Juni";
             Break;
             case 7 : $bulan="Juli";
             Break;
             case 8 : $bulan="Agustus";
             Break;
             case 9 : $bulan="September";
             Break;
             case 10 : $bulan="Oktober";
             Break;
             case 11 : $bulan="November";
             Break;
             case 12 : $bulan="Desember";
             Break;
             }
        return $bulan;
    }

    function hari($hari){
        $hari_ini = $hari;

        switch($hari_ini){
            case 'Sun':
                $hari_ini = "Minggu";
            break;

            case 'Mon':			
                $hari_ini = "Senin";
            break;

            case 'Tue':
                $hari_ini = "Selasa";
            break;

            case 'Wed':
                $hari_ini = "Rabu";
            break;

            case 'Thu':
                $hari_ini = "Kamis";
            break;

            case 'Fri':
                $hari_ini = "Jumat";
            break;

            case 'Sat':
                $hari_ini = "Sabtu";
            break;

            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
        return $hari_ini;
    }

    function gender($jk){
        $jk_i = $jk;

        switch($jk_i){
            case 'male':
                $jk_i = "Laki - laki";
            break;

            case 'female':			
                $jk_i = "Perempuan";
            break;
        }
        return $jk_i;
    }    
 
     function status1($sts){
        $sts_a = $sts;

        switch($sts_a){
            case 'Y':
                $sts_a = "Iya";
            break;

            case 'N':			
                $sts_a = "Tidak";
            break;
        }
        return $sts_a;
    }    

     function status2($sts){
        $sts_a = $sts;

        switch($sts_a){
            case 'Y':
                $sts_a = "Hadir";
            break;

            case 'N':			
                $sts_a = "Tidak Hadir";
            break;
        }
        return $sts_a;
    }    

     function status3($sts){
        $sts_a = $sts;

        switch($sts_a){
            case 'Y':
                $sts_a = "Lolos";
            break;

            case 'N':			
                $sts_a = "Tidak Lolos";
            break;
            
            case 'P':			
                $sts_a = "Ditangguhkan";
            break; 
            
            case 'B':			
                $sts_a = "Dalam Proses";
            break;             
        }
        return $sts_a;
    }    
     function status4($sts){
        $sts_a = $sts;

        switch($sts_a){
            case 'Y':
                $sts_a = "Lunas";
            break;

            case 'N':			
                $sts_a = "Belum Bayar";
            break;
            
            case 'DP':			
                $sts_a = "Bayar DP";
            break; 
            
        }
        return $sts_a;
    }    
   
    
    public function clean($string) {
        $string = strtolower($string);
        $string = str_replace(' ', '-', $string);
        $string = str_replace('(', '', $string);
        $string = str_replace(')', '', $string);
        $string = preg_replace('/[^A-Za-z0-9\-ığşçöüÖÇŞİıĞ]/', '-', $string);
        
        return preg_replace('/-+/', '-', $string);
    }
    

    public function kode($panjang)
    {
        $karakter = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        for($i = 0; $i < $panjang; $i++) {
            $pos = rand(0, strlen($karakter)-1);
            $string .= $karakter{$pos};
        }
        return $string;
    }
    
    function login($username,$password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query =  $this->db->get('m_user');
        return $query->num_rows();
    }

    function data_login($username,$password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('m_user')->row();
    }
}		
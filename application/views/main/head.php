<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/sweetalert2.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/jquery.toast.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/mystyle.css">

    <title>Tugas Saya - Toko Mobil </title>
      
    <script src="<?=base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?=base_url();?>assets/js/popper.min.js"></script>
    <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>  
    <script src="<?=base_url();?>assets/js/sweetalert2.min.js"></script>  
    <script src="<?=base_url();?>assets/js/jquery.toast.min.js"></script>  
    <script src="<?=base_url();?>assets/js/vue.min.js"></script>  
    <script src="<?=base_url();?>assets/js/axios.min.js"></script>  
    <script src="<?=base_url();?>assets/js/vuex.js"></script>  
      
  </head>
  <body>
      
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Toko Mobil</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <span class="navbar-text">
              Admin
            </span>
          </div>
        </nav>      


<style>
.my-custom-scrollbar {
position: relative;
height: 400px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}

</style>
<div id="katalog" class="container" style="margin-top:50px">
    <div class="row">
        <div class="col-md-9">
        </div>
        <div class="col-md-3">
            <form>
              <div class="form-group mb-2">
                <input type="text" class="form-control" id="searchId" v-model="searchId" placeholder="Search">
              </div>
            </form>           
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered">
                <thead>
                    <tr style="background-color:#38a9d3;">
                        <th scope="col">#</th>
                        <th scope="col">Nomor Kerangka</th>
                        <th scope="col">Nomor Polisi</th>
                        <th scope="col">Merek</th>
                        <th scope="col">Tipe</th>
                        <th scope="col">Tahun</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(row, index) in filterItems(katalog)" :key="row.id" >
                        <th scope="row">{{ index + 1}}</th>
                        <td> {{row.no_kerangka}} </td>
                        <td>{{row.no_polisi}}</td>
                        <td>{{row.merek}}</td>
                        <td>{{row.tipe}}</td>
                        <td>{{row.tahun}}</td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" v-on:click="katEdit(row.id)">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm" v-on:click="katDelete(row.id)">Hapus</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <p>Total mobil: {{total}}</p>
        </div>    
        <div class="col-md-4">
              <button type="submit" class="btn btn-success mb-2 " v-on:click="tambah()" style="float:right;padding-left:30px;padding-right:30px;">Tambah</button>       
        </div>    
    </div>

<!-- Modal New-->
<div class="modal fade" id="tambahmobil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Mobil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form>
          <div class="form-group">
            <label for="no_kerangka">Nomor Kerangka</label>
            <input type="text" class="form-control" id="no_kerangka" v-model="no_kerangka">
          </div>
          <div class="form-group">
            <label for="no_polisi">Nomor Polisi</label>
            <input type="text" class="form-control" id="no_polisi" v-model="no_polisi">
          </div>
          <div class="form-group">
            <label for="merek">Merek</label>
            <input type="text" class="form-control" id="merek" v-model="merek">
          </div>
          <div class="form-group">
            <label for="tipe">Tipe</label>
            <input type="text" class="form-control" id="tipe" v-model="tipe">
          </div>
          <div class="form-group">
            <label for="tipe">Tahun</label>
            <input type="text" class="form-control" id="tahun" v-model="tahun">
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary"  v-on:click="tambah_proses()">Simpan</button>
      </div>
    </div>
  </div>
</div> 
    
<!-- Modal Edit-->
<div class="modal fade" id="editmobil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit Mobil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form>
          <div class="form-group">
            <label for="editno_kerangka">Nomor Kerangka</label>
            <input type="text" class="form-control" id="editno_kerangka" v-model="editno_kerangka">
          </div>
          <div class="form-group">
            <label for="editno_polisi">Nomor Polisi</label>
            <input type="text" class="form-control" id="editno_polisi" v-model="editno_polisi">
          </div>
          <div class="form-group">
            <label for="editmerek">Merek</label>
            <input type="text" class="form-control" id="editmerek" v-model="editmerek">
          </div>
          <div class="form-group">
            <label for="edittipe">Tipe</label>
            <input type="text" class="form-control" id="edittipe" v-model="edittipe">
          </div>
          <div class="form-group">
            <label for="edittipe">Tahun</label>
            <input type="text" class="form-control" id="edittahun" v-model="edittahun">
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary"  v-on:click="edit_proses()">Simpan</button>
      </div>
    </div>
  </div>
</div>    
    
</div>

        <script>
                new Vue({
                  el: '#katalog',
                  data() {
                    return {
                        katalog: [],                      
                        no_kerangka: '',                      
                        no_polisi: '',                      
                        merek: '',                      
                        tipe: '',                      
                        tahun: '',
                        total: '',
                        searchId: '',
                        editno_kerangka: '',                      
                        editno_polisi: '',                      
                        editmerek: '',                      
                        edittipe: '',                      
                        edittahun: '',                      
                        kId : ''
                        
                    }
                  },
                    mounted (){
                        this.loadDataAll();
                    },
                  methods: {
                            loadDataAll: function(){
                             axios
                                .get('<?=base_url()?>api/mobil')
                                .then(response => {
                                    this.katalog = response.data.data;                         
                                    this.total = response.data.total;                         
                                })                               
                            },
                    tambah(){ 
                         $('#tambahmobil').modal('show');
                    },
                    tambah_proses(){                    
                            axios({
                              method: 'post',
                              url: '<?=base_url()?>api/saveMobil',
                              responseType: 'stream',
                              data: {
                                no_kerangka : this.no_kerangka,
                                no_polisi : this.no_polisi,
                                merek : this.merek,
                                tipe : this.tipe,
                                tahun : this.tahun
                              }
                            })
                            .then(response => { 
                                var status = response.data.status;
                                if (status==='success'){
                                        Swal.fire({
                                          position: 'top-end',
                                          type: 'success',
                                          title: 'Data has been updated',
                                          showConfirmButton: false,
                                          timer: 1500
                                        })
                                     this.loadDataAll();                                  
                                    } else {
                                        Swal.fire({
                                          position: 'top-end',
                                          type: 'error',
                                          title: 'Data failed to update',
                                          showConfirmButton: false,
                                          timer: 1500
                                        }) 
                                        this.loadDataAll();
                                    }
                        })
                        $("#tambahmobil").modal('hide')
                            },
                    katEdit(b){ 
                         $('#editmobil').modal('show');
                            this.kId = String(b);
                            for(i=0; i < this.katalog.length; i++){
                                if (this.katalog[i].id === this.kId){
                                    this.editno_kerangka = this.katalog[i].no_kerangka;
                                    this.editno_polisi = this.katalog[i].no_polisi;
                                    this.editmerek = this.katalog[i].merek;
                                    this.edittipe = this.katalog[i].tipe;
                                    this.edittahun = this.katalog[i].tahun;
                                }
                            }                         
                    },
                    edit_proses(){            
                        var id = this.kId ;                        
                        axios({
                          method: 'post',
                          url: '<?=base_url()?>api/updateMobil/'+id,
                          responseType: 'stream',
                          data: {
                            no_kerangka : this.editno_kerangka,
                            no_polisi : this.editno_polisi,
                            merek : this.editmerek,
                            tipe : this.edittipe,
                            tahun : this.edittahun
                          }
                        })
                        .then(response => { 
                                var status = response.data.status;
                                if (status==='success'){
                                        Swal.fire({
                                          position: 'top-end',
                                          type: 'success',
                                          title: 'Data has been updated',
                                          showConfirmButton: false,
                                          timer: 1500
                                        })
                                     this.loadDataAll();                                  
                                    } else {
                                        Swal.fire({
                                          position: 'top-end',
                                          type: 'error',
                                          title: 'Data failed to update',
                                          showConfirmButton: false,
                                          timer: 1500
                                        }) 
                                        this.loadDataAll();
                                    }
                        })
                    this.editno_kerangka = '';
                    this.editno_polisi = '';
                    this.editmerek = '';
                    this.edittipe = '';
                    this.edittahun = '';                        
                        $("#editmobil").modal('hide');
                        
                            },
                    katDelete(b){                     
                        Swal.fire({
                          title: 'Are you deleted this?',
                          text: "You won't be able to revert this!",
                          type: 'warning',

                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Yes, do it!'
                            }).then((result) => {
                            if (result.value) {
                                        var id = String(b) ;                        
                                        axios({
                                          method: 'post',
                                          url: '<?=base_url()?>api/deleteMobil/'+id,
                                          responseType: 'stream',
                                          data: ''
                                        })
                                        .then(response => { 
                                            var status = response.data.status;
                                            if (status==='success'){
                                                    Swal.fire({
                                                      position: 'top-end',
                                                      type: 'success',
                                                      title: 'Data has been deleted',
                                                      showConfirmButton: false,
                                                      timer: 1500
                                                    })
                                                 this.loadDataAll();                                  
                                                } else {
                                                    Swal.fire({
                                                      position: 'top-end',
                                                      type: 'error',
                                                      title: 'Data failed to deleted',
                                                      showConfirmButton: false,
                                                      timer: 1500
                                                    }) 
                                                    this.loadDataAll();                                      
                                                }
                                            })       
                                    }
                                     })
                            },
                    filterItems: function(katalog) {
                              var app = this;
                              return katalog.filter(function(row) {
                                let regex = new RegExp('(' + app.searchId + ')', 'i');
                                return row.merek.match(regex);
                              })
                            }                    
                        }
                    })
            
        </script>

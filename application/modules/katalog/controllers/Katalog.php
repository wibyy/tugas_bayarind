<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        if($this->session->userdata('logged') == FALSE){
//            redirect('login');   
//        }
        $this->load->model('m_api');
    }      
	public function index()
	{
		$this->load->view('main/head');
        $this->load->view('konten');
        $this->load->view('main/footer');
	}
    
}
